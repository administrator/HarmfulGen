const { Worker, isMainThread, workerData } = require('node:worker_threads');
const RegistrationAndBuyTask = require('./outlook')
const readline = require("readline")
const fs = require('fs')

let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})
function ask(question) {
    return new Promise(resolve => {
        rl.question(question, (answer) => {
            resolve(answer);
        });
    })
}

function start(data) {
    try {
		new RegistrationAndBuyTask(data).start()
    } catch (err) {
    	start(data, mode)
    }
}

if (isMainThread) {
	ask("[?] Workers: ").then(workers => {
		ask("[?] Threads: ").then(threads => {
			for (let i = 0; i < parseInt(workers); i++) {
				new Worker(__filename, {
					workerData: {
						threads: parseInt(threads),
						mode: parseInt(mode),
					}
				});
			}
		});
	});
} else {
	fs.readFile('./proxies.txt', 'utf8', (err, data) => {
		if (err) {
			console.log("Error reading proxies");
			process.exit()
			return;
		}
		const rawProxies = data.split('\n');
		for (let i = 0; i < workerData.threads; i++) {
			start(rawProxies)
		}
	});
}