const Outlook = require('./outlook')
const Buy = require('./buy')
const fs = require('fs')

const readline = require("readline")
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})
function ask(question) {
    return new Promise((resolve, reject) => {
        rl.question(question, (answer) => {
            resolve(answer)
        })
    })
}

function start(data, mode) {
    try {
        const outlook = new mode(data)
        outlook.start()
    } catch (err) {
    	console.log(err)
    	start(data, mode)
    }
}

const { Worker, isMainThread, workerData } = require('node:worker_threads');

if (isMainThread) {
	const alts = fs.readFileSync('./alts.txt', {encoding:'utf8', flag:'r'}).split("\n")
	ask("[?] Mode (0 - link+buy, 1 - gen): ").then(mode => {
		ask("[?] Workers: ").then(workers => {
			ask("[?] Threads: ").then(threads => {
				const chunkSize = alts.length / parseInt(workers);
				const chunks = []
				for (let i = 0; i < alts.length; i += chunkSize) {
				    chunks.push(alts.slice(i, i + chunkSize));
				}
				for(var i = 0; i < parseInt(workers); i++) {
					const theChunk = chunks[i];
					new Worker(__filename, { 
						workerData: {
							threads: parseInt(threads),
							mode: parseInt(mode),
							alts: theChunk,
						}
					});
				}
			});
		});
	});
} else {
	fs.readFile('./proxies.txt', 'utf8', (err, data) => {
		const rawProxies = []
		if (err) {
			console.log("Error reading proxies");
			process.exit()
			return;
		} else {
			data.split('\n').forEach(proxy => {
				rawProxies.push(proxy.trim())
			});
		}
		let webhook = "https://discord.com/api/webhooks/1008856235421995079/4SjMcgpAYdvZGAS82o__MwicAUH1RGrMyCEzNr0XmRkSMO0HKuDhwrtTd7He1-tKc3vc"
		switch (workerData.mode) {
			case 0:
				let shid = fs.readFileSync('./paket strem.txt', 'utf8').split("\n")
				let xd2 = fs.readFileSync('./ccs.txt', 'utf8').split("\n")
				for (let i = 0; i < workerData.threads; i++) {
					const data = {ccs: xd2, proxyList: shid, alts: workerData.alts, id: 0, key: "", webhook: webhook, limit: 100}
					start(data, Buy)
				}
				break
			case 1:
				let xd = fs.readFileSync('./paket strem.txt', 'utf8').split("\n")
				for (let i = 0; i < workerData.threads; i++) {
					const data = {proxyList2: rawProxies, id: i, key: "", webhook: webhook, limit: 100, proxyList: xd}
					start(data, Outlook)
				}
				break
		}
	});
}