module.exports = {
	"cc_type": "visa",
	"endpoint": "nz",
	"address": {
		"address_line1": "2710 English Ivy Ct",
		"addressCountry": "nz",
		"addressType": "billing",
		"city": "Auckland",
		"country": "nz",
		// "region": "fl",
		"postal_code": "1171"
	},
	"ccexp": ["1", "2025"],
	"gamePassIds": {
		"au": ["CFQ7TTC0KGQ8", "CFQ7TTC0KCBQ"],
		"il": ["CFQ7TTC0KGQ8", "CFQ7TTC0KCPQ"],
		"nz": ["CFQ7TTC0KGQ8", "CFQ7TTC0KCBJ"],
		"us": ["CFQ7TTC0KGQ8", "CFQ7TTC0KCPV"],
		"sg": ["CFQ7TTC0KGQ8", "CFQ7TTC0L3J1"]
	}
}