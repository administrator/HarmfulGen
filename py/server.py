import socket
from http.server import HTTPServer, BaseHTTPRequestHandler
from socketserver import ThreadingMixIn
from unittest.mock import patch

from dortgen_modules.account.funcaptcha import solver_task
from dortgen_modules.account.funcaptcha.bda.browser_agent_fetcher import get_random_bda

proxies = open("proxies.txt", 'r').read().splitlines()

orig_getaddrinfo = socket.getaddrinfo


def getaddrinfo6(host, port, family=0, _type=0, proto=0, flags=0):
    if host == '194.62.41.254' or host == '192.168.1.1' or host == 'p1.creativeproxies.com':
        return orig_getaddrinfo(host=host, port=port, family=socket.AF_INET, type=_type, proto=proto, flags=flags)
    return orig_getaddrinfo(host=host, port=port, family=socket.AF_INET6, type=_type, proto=proto, flags=flags)


def getaddrinfo4(host, port, family=0, _type=0, proto=0, flags=0):
    return orig_getaddrinfo(host=host, port=port, family=socket.AF_INET, type=_type, proto=proto, flags=flags)


class Handler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.end_headers()
        solved = solver_task.solve("https://client-api.arkoselabs.com", "B7D8911C-5CC8-A9A3-35B0-554ACEE604DA",
                                   "https://iframe.arkoselabs.com", proxies)
        print(f"[!] Solved: {solved.split('|')[0]}")
        self.wfile.write(bytes(solved, 'utf-8'))


class ThreadingSimpleServer(ThreadingMixIn, HTTPServer):
    pass


def run():
    server = ThreadingSimpleServer(('0.0.0.0', 1338), Handler)
    server.serve_forever()


if __name__ == '__main__':
    run()
