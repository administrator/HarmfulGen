import random
import secrets
import string

import js2py
from selenium.webdriver import Chrome, ChromeOptions

opts = ChromeOptions()
opts.binary_location = "/opt/google/chrome/chrome"
opts.add_argument("--disable-dev-shm-usage")
opts.add_argument("--no-sandbox")
opts.headless = True
selenium_instance = Chrome(options=opts, executable_path="./chromedriver")
ms_cipher = open("js/ms_cipher.js", "r").read()


def random_string(length):
    return ''.join(random.choice(string.ascii_letters) for _ in range(length))


def random_int(length):
    return ''.join(random.choice(string.digits) for _ in range(length))


def clean_proxies(proxies: list):
    new_proxies = []
    for proxy in proxies:
        new_proxies.append(f"http://{proxy}")
    return new_proxies


def get_random_name():
    return random_string(12)


def get_random_password():
    return f"{''.join(random.choice([str, str.upper])(char) for char in secrets.token_hex(12))}!"


def generate_data():
    f_name = get_random_name()
    l_name = get_random_name()
    day = random.randint(10, 25)
    month = random.randint(3, 9)
    year = random.randint(1980, 1999)
    return {
        "fName": f_name,
        "lName": l_name,
        "day": day,
        "month": month,
        "year": year,
        "redirect": None,
        "tcxt": None,
        "uaid": None,
        "canary": None,
        "key": None,
        "randomNum": None,
        "SKI": None,
        "cipher": None,
        "dfpRequestId": None,
        "encAttemptToken": None,
        "solved": None
    }


def gen_cipher(a, random_num, key):
    return selenium_instance.execute_script(
        f"{ms_cipher}; return genCipher('', '', 'newpwd', '{a}', '{random_num}', '{key}')")


def get_date() -> str:
    return js2py.eval_js("new Date().toISOString()")
