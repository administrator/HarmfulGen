import random
import luhn
import httpx


def generate_card(var0):
    while len(var0) < 15:
        var0 += str(random.randint(0, 9))
    var0 = luhn.append(var0)
    return var0


def gen_letters(param):
    pass



def add_card(account: AccountObject):
    form = {
        "context": "purchase",
        "details": {
            "accountHolderName": f"{gen_letters(6)} {gen_letters(7)}",
            "accountToken": account.cardID,
            "address": account.config.address,
            "cvvToken": account.cvvID,
            "dataCountry": account.config.endpoint,
            "dataOperation": "add",
            "dataType": "credit_card_visa_details",
            "expiryMonth": account.rNum(1, 12).toString(),
            "expiryYear": account.rNum(2023, 2033).toString(),
            "permission": {
                "dataCountry": account.config.endpoint,
                "dataOperation": "add",
                "dataType": "permission_details",
                "hmac": {
                    "algorithm": "hmacsha256",
                    "keyToken": account.genHexString(44),
                    "data": account.genHexString(44)
                },
                "userCredential": account.msAuth
            }
        },
        "paymentMethodCountry": account.config.endpoint,
        "paymentMethodFamily": "credit_card",
        "paymentMethodOperation": "add",
        "paymentMethodResource_id": "credit_card.visa",
        "paymentMethodType": "visa",
        "pxmac": account.genHexString(64),
        "riskData": {
            "dataCountry": account.config.endpoint,
            "dataOperation": "add",
            "dataType": "payment_method_riskData",
            "greenId": account.riskId
        },
        "sessionId": account.guid()
    }
    while 1:
        print(generate_card("446542"))
