import random
import sys

import httpx
from colorama import Fore

from dortgen_modules.account.funcaptcha.solver_task import solve
from dortgen_modules.utils import msgen_util
import names


class Generate:
    def __init__(self, proxy_list, bda_info):
        self.solved_captcha = None
        self.bda_info = bda_info
        self.proxies = msgen_util.clean_proxies(proxy_list)
        self.generated_data = msgen_util.generate_data()
        self.outlookData = self.generated_data
        self.birthday = f'{self.generated_data["day"]}:0{self.generated_data["month"]}:{self.generated_data["year"]}'
        self.email = (names.get_first_name() + random.choice(["_", ""]) + names.get_last_name()
                      + str(random.randint(100000, 384784700))).lower()
        self.password = msgen_util.get_random_password()
        self.proxy = random.choice(self.proxies)
        self.client = httpx.Client(proxies=random.choice(self.proxies),
                                   follow_redirects=True, http2=True)

    def start(self):
        self.load_site()

    def load_site(self):
        redirect = self.client.get("https://signup.live.com/?lic=1")
        body = redirect.text
        self.outlookData["uaid"] = body.split('"clientTelemetry":{"uaid":"')[1].split('"')[0]
        tcxt = body.split('tcxt":"')[1].split('"')[0]
        self.outlookData["tcxt"] = tcxt.replace("\\u002f", "").replace("\\u003a", ":") \
            .replace("\\u0026", "&").replace("\\u003d", "=").replace("\\u002b", "+")

        canary = body.split('"apiCanary":"')[1].split('"')[0]
        self.outlookData["canary"] = canary.replace("\\u002f", "/").replace("\\u003a", ":") \
            .replace("\\u0026", "&").replace("\\u003d", "=").replace("\\u002b", "+")
        self.outlookData["randomNum"] = body.split("var randomNum=\"")[1].split("\"")[0]
        self.outlookData["key"] = body.split("var Key=\"")[1].split("\"")[0]
        self.outlookData["SKI"] = body.split("var SKI=\"")[1].split("\"")[0]
        self.outlookData['cipher'] = msgen_util.gen_cipher(self.password,
                                                           self.outlookData['randomNum'],
                                                           self.outlookData['key'])
        self.main(None)

    def main(self, captcha):
        outlook_headers = {
            "accept": "application/json",
            "accept-encoding": "gzip, deflate, br",
            "accept-language": "en-US,en;q=0.9",
            "cache-control": "no-cache",
            "canary": self.outlookData['canary'],
            "content-type": "application/json",
            "dnt": "1",
            "hpgid": "2006" + str(random.randint(10, 99)),
            "origin": "https://signup.live.com",
            "pragma": "no-cache",
            "scid": "100118",
            "sec-ch-ua": '" Not A;Brand";v="99", "Chromium";v="96", "Google Chrome";v="96"',
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": "Windows",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "tcxt": self.outlookData['tcxt'],
            "uaid": self.outlookData['uaid'],
            "uiflvr": "1001",
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) "
                          "Chrome/96.0.4664.45 Safari/537.36",
            "x-ms-apitransport": "xhr",
            "x-ms-apiversion": "2"
        }
        data = self.gen_body(captcha)
        body = self.client.post(f"https://signup.live.com/API/CreateAccount?lic=1",
                                headers=outlook_headers, json=data).json()
        if 'error' in body:
            error = body['error']
            if '1041' in error['code']:
                self.outlookData['encAttemptToken'] = error['data'].split('encAttemptToken":"')[1].split('"')[0] \
                    .replace('\\u002f', "/").replace('\\u003a', ":").replace('\\u002b', "+") \
                    .replace('\\u0026', "&").replace('\\u003d', "=")
                self.outlookData['dfpRequestId'] = error['data'].split('dfpRequestId":"')[1].split('"')[0]
                self.solved_captcha = httpx.get("http://100.97.153.118:1338/").text
                sys.stdout.write(f"{Fore.GREEN}[+] {Fore.RESET}Solved captcha: "
                                 f"[{self.solved_captcha.split('|')[0]}]\n")
                sys.stdout.flush()
                return self.main(self.solved_captcha)
            elif '1043' in error['code']:
                sys.stdout.write(
                    f"{Fore.LIGHTRED_EX}[-] {Fore.RESET}Bad Captcha: {self.solved_captcha.split('|')[0]}\n")
                sys.stdout.flush()
                del self.outlookData
                pass
            elif '1042' in error['code']:
                sys.stdout.write(f"{Fore.LIGHTRED_EX}[-] {Fore.RESET}SMS Locked: {self.email}\n")
                sys.stdout.flush()
                del self.outlookData
            elif '500' in error['code']:
                sys.stdout.write(f"{Fore.LIGHTRED_EX}[-] {Fore.RESET}Bad IP: {self.email}\n")
                sys.stdout.flush()
                del self.outlookData
        else:
            sys.stdout.write(f"{Fore.GREEN}[+] {Fore.RESET}Created: {self.email}@outlook.com:{self.password}\n")
            sys.stdout.flush()
            del self.outlookData
            open("alts.txt", "a+").write(f"{self.email}@outlook.com:{self.password}\n")

    def gen_body(self, captcha):
        ts = msgen_util.get_date()
        body = {
            "RequestTimeStamp": ts,
            "MemberName": f"{self.email}@outlook.com",
            "CheckAvailStateMap": [f"{self.email}@outlook.com:undefined"],
            "EvictionWarningShown": [],
            "UpgradeFlowToken": {},
            "FirstName": self.outlookData['fName'],
            "LastName": self.outlookData['lName'],
            "MemberNameChangeCount": 1,
            "MemberNameAvailableCount": 1,
            "MemberNameUnavailableCount": 0,
            "CipherValue": self.outlookData['cipher'],
            "SKI": self.outlookData['SKI'],
            "BirthDate": self.birthday,
            "Country": "US",
            "IsOptOutEmailDefault": False,
            "IsOptOutEmailShown": True,
            "IsOptOutEmail": True,
            "LW": True,
            "SiteId": "68692",
            "IsRDM": 0,
            "WReply": None,
            "ReturnUrl": None,
            "SignupReturnUrl": None,
            "uiflvr": 1001,
            "uaid": self.outlookData['uaid'],
            "SuggestedAccountType": "EASI",
            "SuggestionType": "Prefer",
            "scid": 100118,
            "hpgid": 201040
        }
        if captcha is not None:
            body = {
                "RequestTimeStamp": ts,
                "MemberName": f'{self.email}@outlook.com',
                "CheckAvailStateMap": [f'{self.email}@outlook.com:undefined'],
                "EvictionWarningShown": [],
                "UpgradeFlowToken": {},
                "FirstName": self.outlookData['fName'],
                "LastName": self.outlookData['lName'],
                "MemberNameChangeCount": 1,
                "MemberNameAvailableCount": 1,
                "MemberNameUnavailableCount": 0,
                "CipherValue": self.outlookData['cipher'],
                "SKI": self.outlookData['SKI'],
                "BirthDate": self.birthday,
                "Country": "US",
                "IsOptOutEmailDefault": False,
                "IsOptOutEmailShown": True,
                "IsOptOutEmail": True,
                "LW": True,
                "SiteId": "68692",
                "IsRDM": 0,
                "WReply": None,
                "ReturnUrl": None,
                "SignupReturnUrl": None,
                "uiflvr": 1001,
                "uaid": self.outlookData['uaid'],
                "SuggestedAccountType": "EASI",
                "SuggestionType": "Prefer",
                "HType": "enforcement",
                "HSol": captcha,
                "HPId": "B7D8911C-5CC8-A9A3-35B0-554ACEE604DA",
                "encAttemptToken": self.outlookData['encAttemptToken'],
                "dfpRequestId": self.outlookData['dfpRequestId'],
                "scid": 100118,
                "hpgid": 201040
            }
        return body
