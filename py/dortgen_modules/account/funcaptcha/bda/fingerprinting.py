import base64
import json
import random
import secrets
import time
import faker
from dortgen_modules.account.funcaptcha import cipher

faker = faker.Faker()

  #browser_data_json = json.loads(open("browser_data.json").read())


def get_browser_data(user_agent: str) -> str:
    ts = time.time()
    timeframe = int(ts - ts % 21600)
    key = user_agent + str(timeframe)
    downlink = random.randint(0, 500)
    data = [
        {
            "key": "api_type",
            "value": "js"
        },
        {
            "key": "p",
            "value": 1
        },
        {
            "key": "f",
            "value": secrets.token_hex(32)[:32]
        },
        {
            "key": "n",
            "value": base64.b64encode(str(ts).encode('utf-8')).decode('utf-8')
        },
        {
            "key": "wh",
            "value": f"{secrets.token_hex(16)[:32]}|{secrets.token_hex(16)[:32]}"
        },
        {
            "key": "enhanced_fp",
            "value": [
                {
                    "key": "webgl_extensions",
                    "value": secrets.token_hex(32)
                },
                {
                    "key": "webgl_extensions_hash",
                    "value": secrets.token_hex(16)[:32]
                },
                {
                    "key": "webgl_renderer",
                    "value": secrets.token_hex(32)
                },
                {
                    "key": "webgl_vendor",
                    "value": secrets.token_hex(32)
                },
                {
                    "key": "webgl_version",
                    "value": secrets.token_hex(32)
                },
                {
                    "key": "webgl_shading_language_version",
                    "value": secrets.token_hex(32)
                },
                {
                    "key": "webgl_aliased_line_width_range",
                    "value": secrets.token_hex(32)
                },
                {
                    "key": "webgl_aliased_point_size_range",
                    "value": secrets.token_hex(32)
                },
                {
                    "key": "webgl_antialiasing",
                    "value": secrets.token_hex(32)
                },
                {
                    "key": "webgl_bits",
                    "value": secrets.token_hex(32)
                },
                {
                    "key": "webgl_max_params",
                    "value": secrets.token_hex(32)
                },
                {
                    "key": "webgl_max_viewport_dims",
                    "value": secrets.token_hex(32)
                },
                {
                    "key": "webgl_unmasked_vendor",
                    "value": secrets.token_hex(32)
                },
                {
                    "key": "webgl_unmasked_renderer",
                    "value": secrets.token_hex(32)
                },
                {
                    "key": "webgl_vsf_params",
                    "value": secrets.token_hex(32)
                },
                {
                    "key": "webgl_vsi_params",
                    "value": secrets.token_hex(32)
                },
                {
                    "key": "webgl_fsf_params",
                    "value": secrets.token_hex(32)
                },
                {
                    "key": "webgl_fsi_params",
                    "value": secrets.token_hex(32)
                },
                {
                    "key": "webgl_hash_webgl",
                    "value": secrets.token_hex(32)[:32]
                },
                {
                    "key": "user_agent_data_brands",
                    "value": secrets.token_hex(32)
                },
                {
                    "key": "user_agent_data_mobile",
                    "value": False
                },
                {
                    "key": "navigator_connection_downlink",
                    "value": downlink - downlink % 10
                },
                {
                    "key": "navigator_connection_downlink_max",
                    "value": None
                },
                {
                    "key": "network_info_rtt",
                    "value": 50
                },
                {
                    "key": "network_info_save_data",
                    "value": False
                },
                {
                    "key": "network_info_rtt_type",
                    "value": None
                },
                {
                    "key": "screen_pixel_depth",
                    "value": 24
                },
                {
                    "key": "navigator_device_memory",
                    "value": 8
                },
                {
                    "key": "navigator_languages",
                    "value": "en-US,en"
                },
                {
                    "key": "window_inner_width",
                    "value": None
                },
                {
                    "key": "window_inner_height",
                    "value": None
                },
                {
                    "key": "window_outer_width",
                    "value": 3440
                },
                {
                    "key": "window_outer_height",
                    "value": 1403
                },
                {
                    "key": "browser_detection_firefox",
                    "value": False
                },
                {
                    "key": "browser_detection_brave",
                    "value": False
                },
                {
                    "key": "audio_codecs",
                    "value": "{\"ogg\":\"probably\",\"mp3\":\"probably\",\"wav\":\"probably\",\"m4a\":\"maybe\",\"aac\":\"probably\"}"
                },
                {
                    "key": "video_codecs",
                    "value": "{\"ogg\":\"probably\",\"h264\":\"probably\",\"webm\":\"probably\",\"mpeg4v\":\"\",\"mpeg4a\":\"\",\"theora\":\"\"}"
                },
                {
                    "key": "media_query_dark_mode",
                    "value": False
                },
                {
                    "key": "headless_browser_phantom",
                    "value": False
                },
                {
                    "key": "headless_browser_selenium",
                    "value": False
                },
                {
                    "key": "headless_browser_nightmare_js",
                    "value": False
                },
                {
                    "key": "navigator_battery_charging",
                    "value": True
                },
                {
                    "key": "audio_fingerprint",
                    "value": str(random.uniform(34, 126))
                }
            ]
        },
        {
            "key": "fe",
            "value": [
                "DNT:unknown",
                "L:en-US",
                "D:24",
                "PR:1",
                f"S:{random.randint(0, 500)}",
                f"AS:{random.randint(0, 500)}",
                "TO:240",
                "SS:true",
                "LS:true",
                "IDB:true",
                "B:false",
                "ODB:true",
                "CPUC:unknown",
                f"PK:{secrets.token_hex(64)}",
                f"CFP:{secrets.token_hex(128)}",
                "FR:false",
                "FOS:false",
                "FB:false",
                f"JSF:{secrets.token_hex(64)}",
                f"P:{secrets.token_hex(64)}",
                "T:0,false,false",
                "H:4",
                "SWF:false"
            ]
        },
        {
            "key": "ife_hash",
            "value": secrets.token_hex(32)[:32]
        },
        {
            "key": "cs",
            "value": 1
        },
        {
            "key": "jsbd",
            "value": "{\"HL\":2,\"NCE\":true,\"DT\":\"Authentication\",\"NWD\":\"false\",\"DMTO\":1,\"DOTO\":1}"
        }
    ]
    data = cipher.encrypt(json.dumps(data), key)
    data = base64.b64encode(data.encode("utf-8")).decode("utf-8")

    return data


def reduce(var0, var1, var3):
    it = iter(var1)
    var4 = var3
    for element in it:
        var4 = var0(var4, element)
    return var4


def prepare_fingerprint(fingerprint):
    f = []
    keys = fingerprint.keys()
    for key in keys:
        if type(fingerprint[key]) == list:
            f.append(";".join([str(x) for x in fingerprint[key]]))
        else:
            f.append(fingerprint[key])
    return "~~~".join([str(x) for x in f])


def reduce_function(arg0, arg1):
    new_data = (arg0 << 5) - arg0 + ord(arg1[0])
    return new_data & new_data


def cfp_hash(var0):
    return reduce(reduce_function, [char for char in var0], 0)


def prepare_fe(fingerprint):
    fe = []
    keys = fingerprint.keys()
    for key in keys:
        if key == "CFP":
            fe.append(f"{key}:{cfp_hash(fingerprint[key])}")
        elif key == "P":
            fe.append(str(list(map(lambda x: x.split(' ')[0], fingerprint[key]))))
        else:
            fe.append(f'{key}:{fingerprint[key]}')
    return fe
