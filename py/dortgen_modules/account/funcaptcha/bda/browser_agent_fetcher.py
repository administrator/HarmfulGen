import random
import secrets
import string

import faker
from faker.providers import ElementsType
from faker.providers.user_agent import Provider

from dortgen_modules.account.funcaptcha.bda import fingerprinting


class Shid(Provider):
    def shart(
            self,
            version_from: int = 13,
            version_to: int = 63,
            build_from: int = 800,
            build_to: int = 899,
    ) -> str:
        """Generate a Chrome web browser user agent string."""
        saf: str = f"{self.generator.random.randint(531, 536)}.{self.generator.random.randint(0, 2)}"
        tmplt: str = "({0}) AppleWebKit/{1} (KHTML, like Gecko)" " Chrome/{2}.0.{3}.0 Safari/{4}"
        platforms: ElementsType = (
            tmplt.format(
                "Android " + str(random.randint(9, 140)),
                saf,
                self.generator.random.randint(version_from, version_to),
                self.generator.random.randint(build_from, build_to),
                saf,
            ),
        )

        return "Mozilla/5.0 " + self.random_element(platforms)


a = Shid(faker.Faker())


def get_random_bda():
    agent = f"{secrets.token_hex(10)} {a.shart(version_from=200, version_to=111112999)}"
    return {
        "bda": fingerprinting.get_browser_data(agent),
        "agent": agent
    }
