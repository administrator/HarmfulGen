import httpx


class TokenizedObject:
    def __init__(self, token_type: str, data: str):
        self._token_type = token_type
        self._data = data
        self.tokenize()

    def tokenize(self):
        return httpx.post(f"https://tokenization.cp.microsoft.com/tokens/{self._token_type}/getToken",
                          json={"data": self._data}).json()['data']

    def get_data(self):
        return self._data

    def get_token_type(self):
        return self._token_type


class AccountObject:
    def __init__(self, card_id: TokenizedObject, card_expiration: list[str], cvv_id: TokenizedObject):
        self.card_id = card_id
        self.card_expiration = card_expiration
        self.cvv_id = cvv_id

    pass
