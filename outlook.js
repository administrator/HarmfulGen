const {brightCyan, brightGreen, brightYellow} = require('colors')
const {SocksProxyAgent} = require('socks-proxy-agent');
const random_name = require('node-random-name');
const genCipher = require('./cipher');
const request = require('request')
const Purchase = require('./buy')
const fs = require('fs')
process.on('unhandledRejection', () => {});

class RegistrationTask {
    constructor(proxyList) {
        this.email = random_name().replace(" ", "_").toLowerCase() + this.rNum(100, 123746322663).toString()
        this.proxy = proxyList[this.rNum(0, this.proxies.length - 1)]
        this.password = this.randomPassword()
        this.cookieJar = request.jar()
        this.proxies = proxyList;
        this.genData()
    }

    async start() {
        await this.loadSite();
    }

    stop() {
        new Purchase(`${this.email}@outlook.com:${this.password}`, this.proxies)
        let email = random_name().replace(" ", "_").toLowerCase()
        this.email = email.toLowerCase() + this.rNum(100, 123746322663).toString()
        this.password = this.randomPassword()
        this.cookieJar = request.jar()
        this.genData()
        this.outlookData.solve = "";
        this.outlookData.solved = false;
    }

    loadSite() {
        const opts = {
            method: "GET",
            jar: this.cookieJar, gzip: true,
            agent: new SocksProxyAgent(this.proxy)
        }
        request('https://signup.live.com/signup?lic=1', opts, (err, res, body) => {
            if (err) {
                this.loadSite()
            } else {
                try {
                    this.outlookData.uaid = body.split('"clientTelemetry":{"uaid":"')[1].split('"')[0]
                    let tcxt = body.split('"clientTelemetry":{"uaid":"')[1].split(',"tcxt":"')[1].split('"},')[0]
                    this.outlookData.tcxt = tcxt.replaceAll(`\\u002f`, "/").replaceAll(`\\u003a`, ":").replaceAll(`\\u0026`, "&").replaceAll(`\\u003d`, "=").replaceAll(`\\u002b`, "+")

                    let canary = body.split('"apiCanary":"')[1].split('"')[0]
                    this.outlookData.canary = canary.replaceAll(`\\u002f`, "/").replaceAll(`\\u003a`, ":").replaceAll(`\\u0026`, "&").replaceAll(`\\u003d`, "=").replaceAll(`\\u002b`, "+")

                    this.outlookData.randomNum = body.split(`var randomNum="`)[1].split(`"`)[0]
                    this.outlookData.key = body.split(`var Key="`)[1].split(`"`)[0]

                    this.outlookData.SKI = body.split(`var SKI="`)[1].split(`"`)[0]
                    this.main();
                } catch (e) {
                    this.loadSite();
                }
            }
        })
    }

    main() {
        this.registrationTries = 0;
        if (this.outlookData.solved) {
            this.outlookData.body = this.genSolvedBody()
        } else {
            this.outlookData.body = this.genBody()
        }
        const outlookHeaders = {
            "accept": "application/json",
            "accept-encoding": "gzip, deflate, br",
            "accept-language": "en-US,en;q=0.9",
            "cache-control": "no-cache",
            "canary": this.outlookData.canary,
            "content-type": "application/json",
            "dnt": "1",
            "hpgid": "2006" + this.rNum(10, 99).toString(),
            "origin": "https://signup.live.com",
            "pragma": "no-cache",
            "referer": this.outlookData.redir,
            "scid": "100118",
            "sec-ch-ua": `" Not A;Brand";v="99", "Chromium";v="96", "Google Chrome";v="96"`,
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": `"Windows"`,
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "tcxt": this.outlookData.tcxt,
            "uaid": this.outlookData.uaid,
            "uiflvr": "22",
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; MSAppHost/3.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.19044",
            "x-ms-apitransport": "xhr",
            "x-ms-apiversion": "2"
        }
        const opts = {
            body: JSON.stringify(this.outlookData.body),
            headers: outlookHeaders,
            method: "POST",
            jar: this.cookieJar, gzip: true,
            agent: new SocksProxyAgent(this.proxy)
        }
        request(`https://account.live.com/API/CreateAccount?id=80604&Platform=Windows10&skiprps=1&mkt=EN-US&uiflavor=win10host&lic=1&uaid=${this.outlookData.uaid}`, opts, (err, res, body) => {
            if (err) {
                if (this.registrationTries++ < 8) {
                    this.main();
                } else {
                    this.registrationTries = 0;
                    this.loadSite();
                }
            } else {
                try {
                    let loginResp = JSON.parse(body).error;
                    if (!loginResp) {
                        this.status(`Created account: ${this.email}@outlook.com`, 'success')
                        fs.appendFileSync('alts.txt', this.email + "@outlook.com" + ":" + this.password + "\n");
                        this.stop()
                    } else {
                        if (loginResp.code === '1312') {
                            console.log(body)
                            this.status(`SMS Required: ${this.email}@outlook.com`, 'error')
                            this.stop();
                        } else if (loginResp.code !== "1042") {
                            this.status(`SMS Required: ${this.email}@outlook.com`, 'error')
                            this.stop();
                        } else if (loginResp.code === "1041") {
                            this.outlookData.encAttemptToken = loginResp.data.split(`encAttemptToken":"`)[1].split(`"`)[0].replaceAll(`\\u002f`, "/").replaceAll(`\\u003a`, ":").replaceAll(`\\u002b`, "+").replaceAll(`\\u0026`, "&").replaceAll(`\\u003d`, "=");
                            this.outlookData.dfpRequestId = loginResp.data.split(`dfpRequestId":"`)[1].split(`"`)[0];
                            this.loadCaptcha();
                        } else if (loginResp.code === "1043") {
                            this.status(`Bad Captcha: ${this.email}`, 'error')
                            this.cookieJar = request.jar()
                            this.stop();
                        } else {
                            this.loadSite();
                        }
                    }
                } catch (e) {
                    this.loadSite()
                }
            }
        })
    }

    loadCaptcha() {
        request("http://localhost:1338/", {
            method: "GET",
        }, (err, res, body) => {
            if (err) {
                this.loadCaptcha();
                return;
            }
            this.status("Solved Captcha: " + body.split("|")[0], "info");
            this.outlookData.solve = body;
            this.outlookData.solved = true;
            this.main();
        });
    }

    rNum(min, max) { // min and max included 
        return Math.floor(Math.random() * (max - min + 1) + min)
    }

    generateHexString(len) {
        const chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*_';
        let output = '';
        for (let i = 0; i < len; ++i) {
            output += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return output;
    }

    randomPassword() {
        return this.generateHexString(16)
    }

    genData() {
        let fname = random_name({first: true})
        let lname = random_name({last: true})
        let day = this.rNum(10, 25)
        let month = this.rNum(1, 9)
        let year = this.rNum(1980, 1999)
        this.birthday = `${day}:0${month}:${year}`
        this.outlookData = {fname: fname, lname: lname, day: day, month: month, year: year}
    }

    genBody() {
        let ts = new Date();
        this.outlookData.cipher = genCipher("", "", "newpwd", this.password, this.outlookData.randomNum, this.outlookData.key)
        return {
            "BirthDate": this.birthday,
            "CheckAvailStateMap": [`${this.email}@outlook.com:undefined`],
            "CipherValue": this.outlookData.cipher,
            "Country": "CH",
            "EvictionWarningShown": [],
            "FirstName": this.outlookData.fname,
            "LastName": this.outlookData.lname,
            "IsOptOutEmail": true,
            "IsOptOutEmailDefault": false,
            "IsOptOutEmailShown": true,
            "phoneNumber": "+16134481818",
            "IsRDM": 0,
            "LW": true,
            "MemberName": `${this.email}@outlook.com`,
            "MemberNameAvailableCount": 1,
            "MemberNameChangeCount": 1,
            "MemberNameUnavailableCount": 0,
            "RequestTimeStamp": ts,
            "ReturnUrl": null,
            "SKI": this.outlookData.SKI,
            "SignupReturnUrl": null,
            "SiteId": "80604",
            "SuggestedAccountType": "EASI",
            "SuggestionType": "Prefer",
            "UpgradeFlowToken": {},
            "WReply": null,
            "dfpRequestId": "",
            "encAttemptToken": "",
            "hpgid": 200650,
            "scid": 100118,
            "uaid": this.outlookData.uaid,
            "uiflvr": 22
        }
    }

    genSolvedBody() {
        let ts = new Date();
        this.outlookData.cipher = genCipher("", "", "newpwd", this.password, this.outlookData.randomNum, this.outlookData.key)
        return {
            "RequestTimeStamp": ts,
            "MemberName": `${this.email}@outlook.com`,
            "CheckAvailStateMap": [
                `${this.email}@outlook.com:undefined`
            ],
            "EvictionWarningShown": [],
            "UpgradeFlowToken": {},
            "FirstName": this.outlookData.fname,
            "LastName": this.outlookData.lname,
            "MemberNameChangeCount": 1,
            "MemberNameAvailableCount": 1,
            "MemberNameUnavailableCount": 0,
            "CipherValue": this.outlookData.cipher,
            "SKI": this.outlookData.SKI,
            "BirthDate": this.birthday,
            "Country": "US",
            "IsOptOutEmailDefault": false,
            "IsOptOutEmailShown": true,
            "IsOptOutEmail": true,
            "LW": true,
            "SiteId": "68692",
            "IsRDM": 0,
            "WReply": null,
            "ReturnUrl": null,
            "SignupReturnUrl": null,
            "uiflvr": 22,
            "uaid": this.outlookData.uaid,
            "SuggestedAccountType": "EASI",
            "SuggestionType": "Prefer",
            "HType": "enforcement",
            "HSol": this.outlookData.solve,
            "HPId": "B7D8911C-5CC8-A9A3-35B0-554ACEE604DA",
            "encAttemptToken": this.outlookData.encAttemptToken,
            "dfpRequestId": this.outlookData.dfpRequestId,
            "scid": 100118,
            "hpgid": 201040
        }
    }
    status(msg, type) {
        switch (type) {
            case "success":
                console.log(brightGreen(`[✔] ${msg}`));
                break;
            case "status":
                console.log(brightCyan(`[+] ${msg}`));
                break;
            case "info":
                console.log(brightCyan(`[!] ${msg}`));
                break;
            case "error":
                console.log(brightYellow(`[x] ${msg}`));
                break;
        }
    }
}
module.exports = RegistrationTask;